﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AldeanoBehaviour : MonoBehaviour {
	public int capacity = 10;
	private int current;
    public int states;
    public int events;
	public int speed =1;
	GameObject mine;
	GameObject house;
	public int gatheringDistance = 2;
	FSM fsm = new FSM(4,5);

	// Use this for initialization
	void Start () {
        
		fsm.SetRelation (0, 0, 1);
		fsm.SetRelation (1, 1, 2);
		fsm.SetRelation (1, 2, 2);
		fsm.SetRelation (2, 3, 3);
		fsm.SetRelation (3, 4, 0);

		mine = GameObject.FindGameObjectWithTag ("Mine");
		house = GameObject.FindGameObjectWithTag ("House");
        
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log(fsm.GetState());
		switch (fsm.GetState()) {
		case 0:
			transform.LookAt (mine.transform);
			GetComponent<Rigidbody> ().AddForce (transform.forward*speed);
			if(Vector3.Distance(transform.position,mine.transform.position)<gatheringDistance){
                GetComponent<Rigidbody>().ResetInertiaTensor();
                GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
				fsm.SetEvent(0);
			}
			break;
		case 1:
                while (current < capacity)
                {
                    current += mine.GetComponent<Mine>().GetGold(1);
                }
			if(mine.GetComponent<Mine>().CheckEmpty()){
                    break;
			} else {
				fsm.SetEvent(1);
			}
			break;
		case 2:
			transform.LookAt (house.transform);
			GetComponent<Rigidbody> ().AddForce (transform.forward*speed);
			if(Vector3.Distance(transform.position,house.transform.position)<gatheringDistance){
                    GetComponent<Rigidbody>().ResetInertiaTensor();
                    GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                    fsm.SetEvent(3);
			}
			break;
		case 3:
                if (house.GetComponent<House>().CheckFull())
                    break;
			while(current>0)
			current -= house.GetComponent<House>().DepositGold(1);
			if(current == 0)
			fsm.SetEvent(4);
			break;

		}


	}
}
