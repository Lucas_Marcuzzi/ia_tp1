﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM {

	private int[,] fsm;
	private int state = 0;

	// Use this for initialization
	public FSM (int stateammount, int eventammount) {
		fsm = new int[stateammount, eventammount];
		for (int i = 0; i < stateammount; i++)
			for (int e = 0; e < eventammount; e++) {
				fsm [i, e] = -1;
			}
	}

	public void SetRelation(int srcstate,int eut,int dsrstate){
		fsm [srcstate, eut] = dsrstate;
	}

	public int GetState(){
		return state;
	}

	public void SetEvent(int evt){
		if (fsm [state, evt] != -1) {
			state = fsm [state, evt];
		}
	}
}
