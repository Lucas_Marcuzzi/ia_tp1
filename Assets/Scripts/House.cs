﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour {

	private int ammountstored;
	public int capacity = 9999;
    List<GameObject> Notify = new List<GameObject>();

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public int DepositGold(int ammount){
		if (ammountstored + ammount <= capacity) {
			ammountstored += ammount;
            for (int i = 0; i < Notify.Count; i++)
            {
                if (Notify[i].GetComponent<UpdateSign>())
                    Notify[i].GetComponent<UpdateSign>().DoUpdateSign(ammountstored,capacity);
            }
            return ammount;
		} else {
			int exceed = (ammountstored + ammount - capacity);
			ammountstored = capacity;
            for (int i = 0; i < Notify.Count; i++)
            {
                if (Notify[i].GetComponent<UpdateSign>())
                    Notify[i].GetComponent<UpdateSign>().DoUpdateSign(ammountstored, capacity);
            }
            return ammount - exceed;
		}
	}
    public void AddToNotify(GameObject meter)
    {
        Notify.Add(meter);
        if (meter.GetComponent<UpdateSign>())
            meter.GetComponent<UpdateSign>().DoUpdateSign(ammountstored,capacity);
    }
    public bool CheckFull()
    {
        return ammountstored >= capacity;
    }
}
