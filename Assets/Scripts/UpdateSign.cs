﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateSign : MonoBehaviour {

    private Text signText;
    public string header;
    public GameObject ObjectToWatch;

    // Use this for initialization
    void Start () {
        signText = GetComponent<Text>();
        if (ObjectToWatch.GetComponent<Mine>())
        ObjectToWatch.GetComponent<Mine>().AddToNotify(this.gameObject);
        if (ObjectToWatch.GetComponent<House>())
            ObjectToWatch.GetComponent<House>().AddToNotify(this.gameObject);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DoUpdateSign(int value, int cap = -1)
    {
        if(cap == -1)
            signText.text = header + ": " + value;
        else
            signText.text = header + ": " + value + " / " + cap;
    }
}
